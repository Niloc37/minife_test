#pragma once

#include <time.h>
#include <stdio.h>
#include <assert.h>

static int count = 0;
static struct timespec start;
static int line_start = 0;

#define INSTRUMENTATION_BEGIN \
	{ \
		if (count++ > 0) { \
			printf("ERROR, multiple measure at same time !"); \
			exit(1); \
		} \
		line_start = __LINE__; \
		clock_gettime(CLOCK_REALTIME, &start); \
	}
        

#define INSTRUMENTATION_END  \
 	{ \
		static struct timespec stop; \
		clock_gettime(CLOCK_REALTIME, &stop); \
		long seconds = stop.tv_sec - start.tv_sec; \
		long nanoseconds = stop.tv_nsec - start.tv_nsec; \
		double elapsed = seconds + nanoseconds*1e-9; \
		fprintf(stderr, "%s,%d:%d,%d,%f\n", \
                                __FILE__, \
				line_start,  \
				__LINE__, \
				omp_get_max_threads(), \
				elapsed); \
		--count; \
	}


#define REDUCE_THREAD_NB_BEGIN \
	int const thread_nb_saved = omp_get_max_threads(); \
	omp_set_num_threads(4);

#define REDUCE_THREAD_NB_END \
	omp_set_num_threads(thread_nb_saved);
